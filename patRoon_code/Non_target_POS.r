################################Non-target screening of dust and serum with patRoon 2.1.0###############################

##Load library
library(patRoon)

##Set all options


options(patRoon.path.MetFragCL = "~/opt/metfrag/MetFrag2.4.5-CL.jar") # Modify this value to point to the location of the MetFrag jar file

options(patRoon.path.obabel = "~/babel/bin/")

options(patRoon.path.MetFragPubChemLite = ".../patRoon_files/databases/PubChemLite_exposomics_20221125.csv")  #set the location of the PCL database



####Initialitation
#Set the path of the working directory
workPath <- "~../../../patRoon_codes/"
setwd(workPath)

#Set the name of the output folder
output <- "report_NT_POS"

#Load the csv with the data of the mzML files
#Positive ionization files
anaInfo <- read.csv("file_pos_analysis.csv")



###Extract features
library(xcms)

#To find positive features
fList <- findFeatures(anaInfo, "xcms3", param = xcms::CentWaveParam(ppm = 25, peakwidth = c(8, 30), mzdiff = -0.001, mzCenterFun = "wMean", integrate = 1L, snthresh = 10, prefilter = 
                                                                            c(3, 100), noise = 7500, fitgauss = FALSE, firstBaselineCheck = TRUE),verbose = TRUE)


###Group features
library(BiocParallel)
BiocParallel::register(BiocParallel::SerialParam(), default = TRUE)

fGroups <- groupFeatures(fList, "xcms3")

library ("MetaClean") 
fGroups <- filter(fGroups, absMinIntensity = 100,
                  relMinReplicateAbundance = NULL, maxReplicateIntRSD = NULL,
                  blankThreshold = 3, removeBlanks = TRUE,
                  retentionRange = NULL, mzRange = NULL)


###Retrieve MS peak lists
avgPListParams <- getDefAvgPListParams(clusterMzWindow = 0.005)
mslists <- generateMSPeakLists(fGroups, "mzr", maxMSRtWindow = 5, 
                               precursorMzWindow = 0.5, avgFeatParams = 
                               avgPListParams, avgFGroupParams =
                               avgPListParams)

mslists <- filter(mslists, withMSMS = TRUE, absMSIntThr = NULL, absMSMSIntThr = NULL, relMSIntThr = NULL,
                        relMSMSIntThr = NULL, topMSPeaks = NULL, topMSMSPeaks = 25,
                        deIsotopeMS = FALSE, deIsotopeMSMS = FALSE)
						
###Find compound structure candidates
compounds <- generateCompounds(fGroups, mslists, "metfrag", method = "CL",
                               dbRelMzDev = 5, fragRelMzDev = 5, 
                               fragAbsMzDev = 0.002, adduct = "[M+H]+",
                               database = "pubchemlite",
                               scoreTypes = c("fragScore","metFusionScore","score", "individualMoNAScore",
                                              "AnnoTypeCount","ToxicityInfo", "DisorderDisease", "PubMed_Count"),
											maxCandidatesToStop = 500, errorRetries = 200, timeoutRetries = 20) 


		  
###Report results
#Create the csv tables with groups and peak intensities 
reportCSV(fGroups, path = output, reportFeatures = FALSE,
          compounds = compounds, compoundsNormalizeScores = "max")
		  
# Summary of MetFrag Results in a a Single Table 
MFsummary <- as.data.table(compounds)
outputSummary <- paste(output, "MFsummary.csv", sep = "/")
write.csv(MFsummary, outputSummary)

#Plots of every group of features (in the set)
reportPDF(fGroups, path = output, reportFGroups = TRUE, 
          reportFormulaSpectra = TRUE, compounds = compounds, 
          compoundsNormalizeScores = "max",
          MSPeakLists = mslists)