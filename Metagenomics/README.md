# Metagenomics Data Analysis
The code employed for the omics integration can be found in the '' MixOmics''  folder.
The code employed for the pathways analysis can be accessed in the '' Pathways KEGGREST''  folder.

The filtered raw sequence reads are accessible with the following link:
https://www.ncbi.nlm.nih.gov/sra/PRJNA1233217 
