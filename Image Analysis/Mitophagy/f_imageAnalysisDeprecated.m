function [Summary, MitoObjects] = f_imageAnalysis(ch1all, ch2all, ch3all, WellThis, FieldThis, MesFile, PreviewPath)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% vol(ch1all, 0,1000)
%     Summary = table();
% 
    %% segment nuclei
    ch1 = max(ch1all, [], 3); % imtool(ch1, [])
    NucLP = imfilter(ch1, fspecial('gaussian', 55, 11), 'symmetric'); % imtool(NucLP,[])
    %%figure; surf(fspecial('gaussian', 55, 11))
    NucMask = NucLP > 300; % imtool(NucMask,[])
    NucMask = bwareaopen(NucMask, 1500);
    [NucLM, NucCount] = bwlabeln(NucMask); %imtool(NucLM,[])
    NucObjects = regionprops('table', NucLM, ch1, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
    NucSummary = table();
    NucSummary = varfun(@(x) mean(x), NucObjects);
    NucSummary.SumArea = sum(NucMask(:));
    NucSummary.Count = NucCount;
    NucSummary.Properties.VariableNames = strcat('Nuc_', NucSummary.Properties.VariableNames);
% 
% 
%     %% Segment mitochondria
%     ch2 = max(ch2all, [], 3); % imtool(ch2, []) % MTgreen
%     ch3 = max(ch3all, [], 3); % imtool(ch3, []) % TMRE
%     % ch2 and ch3 should have similar intensity
%     chMito = imlincomb(0.9, ch2, 0.1, ch3); %imtool(chMito, [])
% 
%     MitoDoG = imfilter(chMito, fspecial('gaussian', 33, 1) - fspecial('gaussian', 33, 3), 'symmetric'); % imtool(MitoDoG,[])
%     %MitoMask = MitoDoG > 10; % imtool(MitoMask,[])
%     MitoMask = MitoDoG > 50; % imtool(MitoMask,[])
%     MitoMask = bwareaopen(MitoMask, 5);
%     [MitoLM, MitoCount] = bwlabeln(MitoMask); %imtool(MitoLM,[])
%     MitoObjects = regionprops('table', MitoLM, ch3, {'MeanIntensity','Area','MajorAxisLength','MinorAxisLength','Perimeter'});
%     MitoObjects.Well = repmat(WellThis, height(MitoObjects), 1);
%     MitoObjects.Field = repmat(FieldThis, height(MitoObjects), 1);
%     
%     
%     MitoSummary = table();
%     MitoSummary = varfun(@(x) mean(x), MitoObjects);
%     MitoSummary.SumArea = sum(MitoMask(:));
%     MitoSummary.Count = MitoCount;
%     MitoSummary.Properties.VariableNames = strcat('Mito_', MitoSummary.Properties.VariableNames);
% 
%     MetaColumns = table();
%     MetaColumns.Well = WellThis;
%     MetaColumns.Field = FieldThis;
%     %MetaColumns.Timepoint = t;
%     MetaColumns.Mes = {MesFile};
% 
%     %% Collect outputs
%     Summary = [MetaColumns, MitoSummary, NucSummary];

    %% Previews

    imSize = size(ch1all);
    imSize = imSize(1:2);
    [BarMask, BarCenter] = f_barMask(20, 0.10758027143330025, imSize, imSize(1)-50, 50, 20);
    %it(BarMask)

    PreviewRGB = cat(3, imadjust(max(ch3all,[],3),[0 0.02],[0 1]), imadjust(max(ch2all,[],3),[0 0.03],[0 1]), imadjust(max(ch1all,[],3),[0 0.01],[0 1]));
    PreviewRGB = f_imoverlayIris(PreviewRGB, BarMask, [1 1 1]);
    %imtool(PreviewRGB)
    previewPathThis =  [PreviewPath, filesep, WellThis, '_', FieldThis, '.png'];
    imwrite(PreviewRGB, previewPathThis)
    
    
%     NucPreview = f_imoverlayIris(imadjust(ch1, [0 0.01], [0 1]), imdilate(bwperim(NucMask),strel('disk', 1)), [0 0 1]);
%     NucPreview = f_imoverlayIris(NucPreview, BarMask, [1 1 1]);
%     %imtool(NucPreview)
% 
% 
%     MitoPreviewMTgreen = f_imoverlayIris(imadjust(ch2, [0 0.03], [0 1]), bwperim(MitoMask), [1 0 0]);
%     MitoPreviewMTgreen = f_imoverlayIris(MitoPreviewMTgreen, BarMask, [1 1 1]);
%     %imtool(MitoPreviewMTgreen)
% 
%     %MitoPreviewTMRE = f_imoverlayIris(imadjust(ch3, [0 0.03], [0 1]), bwperim(MitoMask), [1 0 0]);
%     MitoPreviewTMRE = f_imoverlayIris(imadjust(ch3, [0 0.2], [0 1]), bwperim(MitoMask), [1 0 0]);
%     MitoPreviewTMRE = f_imoverlayIris(MitoPreviewTMRE, BarMask, [1 1 1]);
%     %imtool(MitoPreviewTMRE)
% 
% 
%     NucPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Nuc.png'];
%     MitoMTgreenPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoMTgreen.png']
%     MitoTMREPreviewPath = [PreviewPath, filesep, WellThis, '_', FieldThis, '_MitoTMRE.png']
% 
%     imwrite(NucPreview, NucPreviewPath)
%     imwrite(MitoPreviewMTgreen, MitoMTgreenPreviewPath)
%     imwrite(MitoPreviewTMRE, MitoTMREPreviewPath)



end


