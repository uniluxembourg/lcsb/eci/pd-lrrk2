%% Collect Linux\Slurm metadata
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Job is running on node:')
[~, node] = system('hostname');
disp(node)
disp('Job is run by user:')
[~, user] = system('whoami');
disp(user)
disp('Current slurm jobs of current user:')
[~, sq] = system(['squeue -u ', user]);
disp(sq)
tic
disp(['Start: ' datestr(now, 'yyyymmdd_HHMMSS')])
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

%% Flags

delete(gcp('nocreate'))
myCluster = parcluster('local');
AvailableWorkers = myCluster.NumWorkers;
if AvailableWorkers >= 24
    pool = parpool(24)
else
    pool = parpool(1)
end

[~, ThisComputer] = system('hostname')
if strfind(ThisComputer, 'hcsfedora001.uni.lux')
    addpath(genpath('/mnt/AtlasHCS/Libraries/hcsforge'))
else
    addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))
    addpath(genpath('/work/projects/lcsb_hcs/Library/hcsIris'))
end


if ~exist('InPath') % if Inpath is provided  via command line, use that one
    %InPath = '/work/projects/lcsb_hcs/Data/JochenOhnmacht/TMRE_IPSneurons_TN/MamasynExpRepeat03_20200909_130231_in';
    %InPath = '/work/projects/lcsb_hcs/Data/JochenOhnmacht/TMRE_IPSneurons_TN/MamasynExpRepeat01_20200828_135057';
    %InPath = '/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Sandro/MFN_template_LysoMito_20210224_ProtectMove_20210224_122721/AssayPlate_PerkinElmer_CellCarrierUltra96';
    InPath = '/mnt/irisgpfs/projects/lcsb_hcs/Data/SandroPereira/SP_SH_MitotrackerG_LysotrackerDR_Hoechst_N3_MitophagyNeurons_20240228_171149_in';
    
end

MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);

if ~exist('OutPath') % if Outpath is provided  via command line, use that one
    %OutPath = '/work/projects/lcsb_hcs/Data/JochenOhnmacht/TMRE_IPSneurons_TN/MamasynExpRepeat03_20200909_130231_out';
    %OutPath = '/mnt/AtlasHCS/HCS_Platform/Data/SylvieDelcambre/Mitophagy/MFN_template_LysoMito_20210224_ProtectMove_20210224_122721_out';
    OutPath = '/mnt/irisgpfs/projects/lcsb_hcs/Data/SandroPereira/SP_SH_MitotrackerG_LysotrackerDR_Hoechst_N3_MitophagyNeurons_20240228_171149_out_inter';
end

%% Prepare folders
mkdir(OutPath)
PreviewPath = [OutPath, filesep, 'Previews'];
mkdir(PreviewPath)

%% Log
 f_LogDependenciesLinux(mfilename, OutPath)

%% get csv
% [~, csvpath] = system(['find ' InPath '/*.csv']);
% csvlist = readtable(csvpath(1:end-1), 'delimiter', 'tab');
% WellsToAnalyse = table2cell(csvlist(csvlist.TMRE == 1, 'well'));

%% Load Metadata
ObjectsAll = {};
%MetaData = f_CV8000_getChannelInfo(InPath, MesPath);
InfoTable = MetaData.InfoTable{:};
Wells = unique(InfoTable.Well);
fieldProgress = 0;
for w = 1:numel(Wells)
    WellThis = Wells{w};
    InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, WellThis),:);
    FieldsThisWell = unique(InfoTableThisWell.Field);
    for f = 1:numel(FieldsThisWell)
        fieldProgress = fieldProgress + 1;
        FieldThis = FieldsThisWell{f};
        InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, FieldsThisWell{f}),:);
        ChannelsThisField =  unique(InfoTableThisField.Channel);
        ImPaths = cell(1, numel(ChannelsThisField));
        for c = 1:numel(ChannelsThisField)
            ChannelThis = ChannelsThisField{c};
            InfoTableThisChannel = InfoTableThisField(strcmp(InfoTableThisField.Channel,ChannelThis),:);
            InfoTableThisChannel = sortrows(InfoTableThisChannel, 'Plane', 'ascend');
            chThisPaths = cell(numel(ChannelsThisField),1);
            for p = 1:height(InfoTableThisChannel)
                chThisPaths{p} = InfoTableThisChannel{p, 'file'}{:};
                %for t = 1:height()
            end
            ImPaths{c} = chThisPaths;
            MesFile = MetaData.MeasurementSettingFileName;
        end
       FieldMetaData{fieldProgress} = {ImPaths, MesFile, Wells{w}, FieldsThisWell{f}};
    end
end

disp('Debug point')
ListIms = cell2table(vertcat(FieldMetaData{:}));

%%
reduceOverfitting = false
if reduceOverfitting
        i=6; % B03_003
        FieldThis = FieldMetaData{i}{4};
        WellThis = FieldMetaData{i}{3};

        ch1files = sort(FieldMetaData{i}{1}{1});
        ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
        ch1l = cat(3,ch1Collector{:}); % vol(ch1, 0, 2000) Hoechst it(max(ch1, [], 3))

        ch2files = FieldMetaData{i}{1}{2};
        ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
        ch2l = cat(3,ch2Collector{:}); % vol(ch2, 0, 2000) MTgreen

        ch3files = FieldMetaData{i}{1}{3};
        ch3Collector = cellfun(@(x) imread(x), ch3files, 'UniformOutput', false);
        ch3l = cat(3,ch3Collector{:}); % vol(ch3, 0, 2000) LysoDR

        i=18; % B07_003
        FieldThis = FieldMetaData{i}{4};
        WellThis = FieldMetaData{i}{3};

        ch1files = sort(FieldMetaData{i}{1}{1});
        ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
        ch1r = cat(3,ch1Collector{:}); % vol(ch1, 0, 2000) Hoechst it(max(ch1, [], 3))

        ch2files = FieldMetaData{i}{1}{2};
        ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
        ch2r = cat(3,ch2Collector{:}); % vol(ch2, 0, 2000) MTgreen

        ch3files = FieldMetaData{i}{1}{3};
        ch3Collector = cellfun(@(x) imread(x), ch3files, 'UniformOutput', false);
        ch3r = cat(3,ch3Collector{:}); % vol(ch3, 0, 2000) LysoDR
        
        MesFile = FieldMetaData{i}{2};
        
        ch1 = [ch1l; ch1r]; %vol(ch1, 0, 2000)
        ch2 = [ch2l; ch2r]; %vol(ch2, 0, 2000)
        ch3 = [ch3l; ch3r]; %vol(ch3, 0, 2000)
        
        [Objects, MitoObjects] = f_imageAnalysis(ch1, ch2, ch3, WellThis, FieldThis, MesFile, PreviewPath);
        ObjectsAll{i} = Objects;
        MitoObjectsAll{i} = MitoObjects;
end
%%

for i = 1:numel(FieldMetaData)% C02 --> i = 428 
%for i = 2
%parfor i = 1:numel(FieldMetaData)% C02 --> i = 55 
%for i = 166 % E02 field 04
%for i = 176 % E03 field 05
%parfor i = 1:numel(FieldMetaData)
%parfor i = 1:2
    try
        i
        FieldThis = FieldMetaData{i}{4};
        WellThis = FieldMetaData{i}{3};

        ch1files = sort(FieldMetaData{i}{1}{1});
        ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
        ch1 = cat(3,ch1Collector{:}); % vol(ch1, 0, 2000) MitotrackerG it(max(ch1, [], 3))

        ch2files = FieldMetaData{i}{1}{2};
        ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
        ch2 = cat(3,ch2Collector{:}); % vol(ch2, 0, 2000) LysotrackerDR

        ch3files = FieldMetaData{i}{1}{3};
        ch3Collector = cellfun(@(x) imread(x), ch3files, 'UniformOutput', false);
        ch3 = cat(3,ch3Collector{:}); % vol(ch3, 0, 2000) Hoechst

        MesFile = FieldMetaData{i}{2};
        %WellThis = FieldMetaData{i}{3};
        %FieldThis = FieldMetaData{i}{4};
        [Objects, MitoObjects] = f_imageAnalysis(ch1, ch2, ch3, WellThis, FieldThis, MesFile, PreviewPath);
        ObjectsAll{i} = Objects;
        MitoObjectsAll{i} = MitoObjects;

    catch errorThis
       disp(errorThis)
       continue
    end
end

Data = vertcat(ObjectsAll{:});
save([OutPath, filesep, 'data.mat'], 'Data');
writetable(Data, [OutPath, filesep, 'data.csv'])

MitoObjectsAll = vertcat(MitoObjectsAll{:});
writetable(MitoObjectsAll, [OutPath, filesep, 'MitoObjectsAll.csv'])
disp('Script completed successfully')
