%function [Summary, MitoObjects, LysoObjectsCell, ObjectsCell] = f_imageAnalysis(chNuc, chMito, chLyso, WellThis, FieldThis, MesFile, PreviewPath)
function [LysoObjectsCell, ObjectsField] = f_imageAnalysis(chMito, chLyso, chNuc, WellThis, FieldThis, MesFile, PreviewPath)



        %% Lyso
        ch2Corr = chLyso - medfilt3(chLyso,[5,5,3]);
        ch2Corr = medfilt3(ch2Corr, [3,3,1]);
        %ch2Corr = f_ImAdjust(ch2Corr, 0.9999);
        % vol(ch2Corr, 0, 100) % vol(chLyso, 0, 2000)
        
        %% Mito
        chMitoCorr = chMito - medfilt3(chMito,[5,5,3]);
        chMitoCorr = medfilt3(chMitoCorr, [3,3,1]);
        %ch3Corr = f_ImAdjust(ch3Corr, 0.9999);
        % vol(ch3Corr, 0, 200)vol(chMito, 0, 2000)

        %% Find Lyso
        %Lyso_DoG = imfilter(double(ch2Corr), fspecial('gaussian', 11, 1), 'symmetric') - imfilter(double(ch2Corr), fspecial('gaussian', 11, 1.75), 'symmetric');
        Lyso_DoG = imfilter(double(ch2Corr), fspecial('gaussian', 11, 1), 'symmetric') - imfilter(double(ch2Corr), fspecial('gaussian', 11, 2), 'symmetric');
%         Lyso_DoG = im2uint16(mat2gray(Lyso_DoG)); %imtool(max(Lyso_DoG,[],3))
%         Lyso_DoG = Lyso_DoG - uint16(mean(Lyso_DoG));
        %vol(Lyso_DoG, 0, 30000)
        %LysoMaskFromDoG = Lyso_DoG > 5;% vol(LysoMaskFromDoG, 0, 1)
        %LysoMaskFromDoG = Lyso_DoG > 4;% vol(LysoMaskFromDoG, 0, 1)
        LysoMaskFromDoG = Lyso_DoG > 100;% vol(LysoMaskFromDoG, 0, 1)
        LysoMask = LysoMaskFromDoG;
        LysoMaskGlobal = ch2Corr > 20; % vol(LysoMaskGlobal)
        LysoMask = LysoMask & LysoMaskGlobal;
        LysoMask = imopen(LysoMaskFromDoG, strel('disk', 1));
        LysoMask = bwareaopen(LysoMask, 9, 4);
        %LysoMask = bwareafilt(LysoMask, [9, 500]);
        LysoMask = logical(f_RemoveBigObjects(LysoMask, 500));
        LysoMask = imreconstruct(LysoMask, LysoMaskFromDoG);
        
        %vol(LysoMask)

        %% Find nuclei

        NucIm = medfilt3(chNuc, [5,5,3]); % vol(NucIm, 0, 500)
        ThresholdLow = quantile(NucIm(:), 0.1)
        ThresholdHigh = quantile(NucIm(:), 0.999)
        %ThresholdSteps = linspace(double(ThresholdLow), double(ThresholdHigh), 50)
        ThresholdSteps = linspace(double(ThresholdLow), double(ThresholdHigh), 25)

        for ii = 1:size(ThresholdSteps, 2)
            NucMask = NucIm > ThresholdSteps(ii);
            Metric(ii) = sum(NucMask(:));
            if ii > 1
                Delta(ii) = Metric(ii) - Metric(ii-1);
                if ii > 2
                    DeltaDelta(ii) = Delta(ii) - Delta(ii-1);
                end
            end
        end
        
        ThresholdIndex = find(DeltaDelta == max(DeltaDelta));
%         figure
%         plot(Metric)
%         plot(Delta)
%         plot(DeltaDelta)
        NucMaskIncludingApoptotic = NucIm > ThresholdSteps(ThresholdIndex);
        
        NucMask = bwareaopen(NucMaskIncludingApoptotic, 5000); % vol(NucMask, 0,1)
        
        
        %% Update LysoMask ##############################
        LysoMask = LysoMask & ~NucMaskIncludingApoptotic;
        %% #############################################
        %vol(NucMask)

        %% Find Mito
        Mito_DoG = imfilter(double(chMitoCorr), fspecial('gaussian', 11, 1)) - imfilter(double(chMitoCorr), fspecial('gaussian', 11, 1.75), 'symmetric');
        %vol(Mito_DoG, 0, 200, 'hot')
        MitoMask = Mito_DoG > 30;
        MitoMask = MitoMask & chMitoCorr > 50; % vol(chMitoCorr, 0, 600)
        MitoMask = bwareaopen(MitoMask, 9, 4);
        %vol(MitoMask)
       

        %% Find Coloc

        %% Colocalization

        ColocMitoLyso = MitoMask & LysoMask; % vol(ColocMitoLyso)

        Objects = table();
        %Objects.FileName = files(i);
        Objects.Well = {WellThis};
        Objects.Field = {FieldThis};
        
        Objects.ColocPixels = sum(ColocMitoLyso(:));
        Objects.MitoPixels = sum(MitoMask(:));
        Objects.LysoPixels = sum(LysoMask(:));
        Objects.LysoPixelIntensitySum = sum((chLyso(LysoMask)));
        Objects.LysoPixelMeanIntensity = Objects.LysoPixelIntensitySum / Objects.LysoPixels;
        [~, Objects.MitoCount] = bwlabeln(MitoMask, 26);
        [~, Objects.LysoCount] = bwlabeln(LysoMask, 26);
        [~, Objects.ColocCount] = bwlabeln(ColocMitoLyso, 26);
        Objects.NucArea = sum(NucMask(:));
        [~, Objects.NucCount] = bwlabeln(NucMask, 26);


        %% Single organelle analysis
        LysoLM = bwlabeln(LysoMask);%vol(LysoLM,0, 200)
        LysoObjects = regionprops('table', LysoLM, {'Area', 'PixelIdxList'});
        Celldummy = cell(height(LysoObjects),1);  
        FileColumn = cellfun(@(x) [WellThis, '_', FieldThis], Celldummy, 'UniformOutput', false);
        FileTable = cell2table(FileColumn);
        FileTable.Properties.VariableNames{end} = 'File';
        FileTable.Well = repmat({WellThis}, height(FileTable), 1);
        FileTable.Field = repmat({FieldThis}, height(FileTable), 1);
        
        LysoObjectsCell = [FileTable, LysoObjects(:,'Area')];
        % LysoObjectsCell = Iris_AnnotateTable(LysoObjectsCell, Layout, {'Barcode', 'CellLine', 'ExperimentalCondition'});
        
        %%
        plotting = 0
        if plotting
            figure
            histogram(LysoObjects.Area, 200)
        end
        
        LysoObjectsBig = LysoObjects(LysoObjects.Area > 1000, :);
        LysoMaskBig = f_Create_Mask_from_ObjectList_Pixel_IDX(LysoObjectsBig, 'PixelIdxList', LysoMask);
        %vol(LysoMaskBig)
        
        LysoObjectsSmall = LysoObjects(LysoObjects.Area <= 1000, :);
        LysoMaskSmall = f_Create_Mask_from_ObjectList_Pixel_IDX(LysoObjectsSmall, 'PixelIdxList', LysoMask);
        %vol(LysoMaskSmall)
        
        % Shape
        Conn6Strel = {};
        Conn6Strel{1} = [0 0 0; 0 1 0; 0 0 0];
        Conn6Strel{2} = [0 1 0; 1 1 1; 0 1 0];
        Conn6Strel{3} = [0 0 0; 0 1 0; 0 0 0];
        Conn6Strel = logical(cat(3, Conn6Strel{:}));
        MitoErodedMask = imerode(MitoMask, Conn6Strel);
        MitoPerimMask = (MitoMask - MitoErodedMask) > 0;

        RunSkel = false
        if RunSkel
            % skeleton
            skel = Skeleton3D(MitoMask);    

            [AdjacencyMatrix, node, link] = Skel2Graph3D(skel,0); % 0 for keeping all branches 
            %it(AdjacencyMatrix)

            %% Single mitochondria analysis
            MitoLM = bwlabeln(MitoMask);
            NodeMask = zeros(size(MitoMask), 'logical');
            for no = 1:size(node, 2)
                NodeMask(node(no).idx) = 1;
            end
            % vol(NodeMask, 0,1)
            LinkMask = zeros(size(MitoMask), 'logical');
            for li = 1:size(link, 2)
                LinkMask(link(li).point) = 1;
            end
            %vol(LinkMask, 0,1)
            MitoObjects = regionprops('table', MitoLM, {'Area', 'SubarrayIdx', 'Image'});
            % Count Nodes per mitochondrium
            MitoObjectsPlusNodes = f_SingleCellSubMask3D(MitoObjects, NodeMask);
            Nodes = [];
            for n = 1:height(MitoObjectsPlusNodes)
                NodeLMThis = bwlabeln(MitoObjectsPlusNodes{n,'SubMasks'}{:}, 26);
                Nodes(n,1) = max(NodeLMThis(:));
                %vol(NodeLMThis)
            end
            %NodeCountPerMito = rowfun(@(x) max(bwlabeln(x{:}(:), 26)), MitoObjectsPlusNodes, 'InputVariables', {'SubMasks'});

            % Count Links per mitochondrium
            MitoObjectsPlusLinks = f_SingleCellSubMask3D(MitoObjects, LinkMask);
            Links = [];
            for m = 1:height(MitoObjectsPlusLinks)
                LinkLMThis = bwlabeln(MitoObjectsPlusLinks{m,'SubMasks'}{:}, 26);
                Links(m,1) = max(LinkLMThis(:));
                %vol(LinkLMThis)
            end
            %LinkCountPerMito = rowfun(@(x) {bwlabeln(x{:}(:), 26)}, MitoObjectsPlusLinks, 'InputVariables', {'SubMasks'});
            %LinkCountPerMito = rowfun(@(x) max(bwlabeln(x{:}(:), 26)), MitoObjectsPlusLinks, 'InputVariables', {'SubMasks'});

            MitoObjects.NodeCount = Nodes;
            MitoObjects.LinkCount = Links;

            % vol(skel + 2*LinkMask + 4*NodeMask, 0, 7, 'hot')
            % vol(MitoMask + skel + LinkMask, 0, 3, 'hot')

            Objects.MedianLinks = median(Links);
            Objects.MedianNodes = median(Nodes);
            Objects.MedianMitoPixels = median(MitoObjects.Area);
            Objects.MeanMitoPixels = mean(MitoObjects.Area);
        end        
        %% Additional feature images and vectors
        MitoBodyLabelIm = bwlabeln(MitoErodedMask, 26);
        %Objects.NodeDegreeVector = {sum(AdjacencyMatrix, 1)};
        % Lysosomes
        Objects.LysoSmallCount = height(LysoObjectsSmall);
        Objects.LysoBigCount = height(LysoObjectsBig);
        Objects.LysoSizeMean = mean(LysoObjects.Area);
        Objects.LysoSizeMedian = median(LysoObjects.Area);
        Objects.LysoSizeStd = std(LysoObjects.Area);
        
        if RunSkel
            % Erosion derived
            Objects.MitoSkelPixels = sum(skel(:));
            Objects.MitoPerimPixels = sum(MitoPerimMask(:));
            Objects.MitoBodyPixels = sum(MitoErodedMask(:)); 
            Objects.MitoBodyCount = max(MitoBodyLabelIm(:)); % Needed for invagination feature
            Objects.MitoShapeBySurface = Objects.MitoBodyPixels / Objects.MitoPerimPixels; % Roundness feature
            Objects.MitoBodycountByMitocount = Objects.MitoBodyCount / Objects.MitoCount; % Invagination feature
        end
        %%
        % Nuclei features
        Objects.NucleiPixels = sum(NucMask(:));
        [~, Objects.NucleiCount] = bwlabeln(NucMask);
        if RunSkel
            % Skeleton derived
            Objects.TotalNodeCount = size(node, 2);
            Objects.AverageNodePerMito = Objects.TotalNodeCount / Objects.MitoCount;
            Objects.TotalLinkCount = size(link, 2);
            Objects.NodesPerMitoMean = Objects.TotalNodeCount / Objects.MitoCount;
            Objects.LinksPerMitoMean = Objects.TotalLinkCount / Objects.MitoCount;
            %Objects.AverageNodeDegree = mean(Objects.NodeDegreeVector{:}); %average lenght of branch in pixels
            %Objects.MedianNodeDegree = median(Objects.NodeDegreeVector{:});
            %Objects.StdNodeDegree = std(Objects.NodeDegreeVector{:});
            %Objects.MadNodeDegree = mad(Objects.NodeDegreeVector{:}, 1);
        end
        Objects.ColocPixelsByMitoPixels = Objects.ColocPixels / Objects.MitoPixels;
        ObjectsField = Objects;
        %ObjectsField = Iris_AnnotateTable(ObjectsField, Layout, {'Barcode', 'CellLine', 'ExperimentalCondition'});

        %% Previews
        % Scalebar
        imSize = size(MitoMask);
        [BarMask, BarCenter] = f_barMask(20, 0.10758027143330025, imSize, imSize(1)-100, 100, 20);
        %it(BarMask)
        
        MidPlane = floor((size(MitoMask, 3) / 2));
        
        PreviewRGB = cat(3, imadjust(max(chLyso, [], 3),[0 0.09],[0 1]), imadjust(max(chMito, [], 3),[0 0.05],[0 1]), imadjust(max(chNuc, [], 3),[0 0.007],[0 1]));
        PreviewRGB = imoverlay(PreviewRGB, BarMask, [1 1 1]);
        %imtool(PreviewRGB)

        %PreviewMito = imoverlay(imadjust(chMito(:,:,MidPlane),[0 0.02],[0 1]), bwperim(MitoMask(:,:,MidPlane)), [0 1 0]);
        PreviewMito = imoverlay(imadjust(max(chMito,[],3), [0 0.07],[0 1]), bwperim(max(MitoMask,[],3)), [0 1 0]);
        PreviewMito = imoverlay(PreviewMito, BarMask, [1 1 1]);
        %imtool(PreviewMito)

        %PreviewLyso = imoverlay(imadjust(max(chLyso,[],3),[0 0.02],[0 1]), bwperim(max(LysoMask,[],3)), [1 0 0]);
        PreviewLyso = imoverlay(imadjust(max(chLyso,[],3),[0 0.2],[0 1]), bwperim(max(LysoMask,[],3)), [1 0 0]);
        PreviewLyso = imoverlay(PreviewLyso, bwperim(max(NucMask, [], 3)), [0 0 1]);
        PreviewLyso = imoverlay(PreviewLyso, BarMask, [1 1 1]);
        %imtool(PreviewLyso)
        
        
        %PreviewLyso = imoverlay(imadjust(max(chLyso,[],3),[0 0.02],[0 1]), bwperim(max(LysoMask,[],3)), [1 0 0]);
        PreviewLysoAuto = imoverlay(imadjust(max(chLyso,[],3)), bwperim(max(LysoMask,[],3)), [1 0 0]);
        PreviewLysoAuto = imoverlay(PreviewLysoAuto, bwperim(max(NucMask, [], 3)), [0 0 1]);
        PreviewLysoAuto = imoverlay(PreviewLysoAuto, BarMask, [1 1 1]);
        %imtool(PreviewLysoAuto)

        PreviewColoc = cat(3, ((2^16)-1) .* max(LysoMask, [], 3), ((2^16)-1) .* max(MitoMask, [], 3), zeros(size(max(MitoMask,[],3)), 'uint16'));
        PreviewColoc = imoverlay(PreviewColoc, BarMask, [1 1 1]);
        %imtool(PreviewColoc)
        
        %% 
        PreviewLysoClass = imoverlay(imadjust(chLyso(:,:,MidPlane),[0 0.035],[0 1]), bwperim(LysoMaskBig(:,:,MidPlane)), [1 1 0]);
        PreviewLysoClass = imoverlay(PreviewLysoClass, bwperim(LysoMaskSmall(:,:,MidPlane)), [0 0 1]);
        PreviewLysoClass = imoverlay(PreviewLysoClass, BarMask, [1 1 1]);
        %imtool(PreviewLysoClass)
        
        filenameRGB = [PreviewPath, filesep, WellThis, '_', FieldThis, '_RGB.png']
        filenameMito = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Mito.png']
        filenameLyso = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Lyso.png']
        filenameLysoAdj = [PreviewPath, filesep, WellThis, '_', FieldThis, '_LysoAdj.png']
        filenameColoc = [PreviewPath, filesep, WellThis, '_', FieldThis, '_Coloc.png']

        imwrite(PreviewRGB, filenameRGB);
        imwrite(PreviewMito, filenameMito);            
        imwrite(PreviewLyso, filenameLyso);   
        imwrite(PreviewLysoAuto, filenameLysoAdj);  
        imwrite(PreviewColoc, filenameColoc);   


end

