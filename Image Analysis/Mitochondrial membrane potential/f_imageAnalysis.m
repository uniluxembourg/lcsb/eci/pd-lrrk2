%function [Objects] = ImageAnalysisBrunoSNCAtriplicateCV8000Horst(ch1_H, ch2_MTgreen, ch3TMRE, WellThis, FieldThis, MesFile, PreviewPath)
function [Objects] = ImageAnalysisBrunoSNCAtriplicateCV8000Horst(ch1_H, ch2_MTgreen, ch3TMRE, WellThis, FieldThis, MesFile, PreviewPath, Layout)
                       
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

%     vol(ch1_H, 0, 10000)
%     vol(ch2_MTgreen, 0, 10000)
%     vol(ch3TMRE, 0, 10000)

    %% Segment mitochondria
    %% Median
    %MitoDoGTom20 = imfilter(ch2_Tom20, fspecial('gaussian', 61, 1), 'symmetric') - imfilter(ch2_Tom20, fspecial('gaussian', 61, 20), 'symmetric'); % vol(MitoDoGTom20, 0, 2000, 'hot')
    chMito = imlincomb(0.9, ch2_MTgreen, 0.1, ch3TMRE); %vol(chMito, 0, 10000)
    MitoDoG = imfilter(chMito, fspecial('gaussian', 61, 1), 'symmetric') - imfilter(chMito, fspecial('gaussian', 61, 3), 'symmetric'); % vol(MitoDoG, 0, 1000, 'hot')
    MitoDoGthreshold = 125;
    MitoMaskTomLocal = MitoDoG > MitoDoGthreshold;%50; %vol(MitoMaskTomLocal)
    MitoRawthreshold = quantile(chMito(MitoMaskTomLocal), 0.01);
    MitoMaskTomGlobal = medfilt3(chMito) > MitoRawthreshold; % vol(MitoMaskTomGlobal)
    MitoMask = MitoMaskTomLocal & MitoMaskTomGlobal;%vol(MitoMask)
    MitoMask = bwareaopen(MitoMask, 7);%vol(MitoMask)
    
    %% Segment Nuclei
    NucleiDoG = imfilter(double(ch1_H), fspecial('gaussian', 300, 10), 'symmetric') - imfilter(double(ch1_H), fspecial('gaussian', 300, 100), 'symmetric'); %vol(NucleiDoG, 0, 2000)
    NucDoGMask = NucleiDoG > 800; %vol(NucDoGMask)
    MinThreshold = 200;% vol(ch1_H, 0, 2000)
    MaxThreshold = 2000;
    ThresholdStep = 50;
    display = 0;
    [PreNucMask, ThresholdNuclei] = f_IterativeThresholding(ch1_H, MinThreshold, MaxThreshold, ThresholdStep, display);
    PreNucMask = PreNucMask & NucDoGMask; %vol(PreNucMask)
    ThresholdNucleiRefined = quantile(ch1_H(PreNucMask), 0.1);
    NucMask = medfilt3(ch1_H) > ThresholdNucleiRefined;
    NucMask = bwareaopen(NucMask, 5000); %vol(NucMask)

    %% Collect features

    MitoLabelMatrix = bwlabeln(MitoMask, 26); %vol(MitoLabelMatrix)
    MitoObjects = regionprops('table', MitoLabelMatrix, ch2_MTgreen, {'Area', 'MeanIntensity'}); % doc regionprops

    % Shape
    Conn6Strel = {};
    Conn6Strel{1} = [0 0 0; 0 1 0; 0 0 0];
    Conn6Strel{2} = [0 1 0; 1 1 1; 0 1 0];
    Conn6Strel{3} = [0 0 0; 0 1 0; 0 0 0];
    Conn6Strel = logical(cat(3, Conn6Strel{:}));
    MitoErodedMask = imerode(MitoMask, Conn6Strel);
    MitoPerimMask = (MitoMask - MitoErodedMask) > 0;% vol(MitoPerimMask)

    doSkel = false
    if doSkel
        % skeleton  
        skel = Skeleton3D(MitoMask);
        skelP = cat(3, zeros(size(skel), 'logical'), skel, zeros(size(skel), 'logical')); % vol(skelP)

        [AdjacencyMatrix, node, link] = Skel2Graph3D(skel,0); % 0 for keeping all branches  
        %save([AdjMatPath, filesep, num2str(i)], 'AdjacencyMatrix', 'node', 'link', 'WellThis', 'FieldThis');
        try
            save([AdjMatPath, filesep, WellThis, '_', FieldThis], 'AdjacencyMatrix', 'node', 'link', 'WellThis', 'FieldThis', '-v7.3');
        catch error
            save([AdjMatPath, filesep, WellThis, '_', FieldThis], 'error', '-v7.3');
        end
    end

    %% Additional feature images and vectors
    MitoBodyLabelIm = bwlabeln(MitoErodedMask, 6);

    Objects = table();
    Objects.Well = {WellThis};
    Objects.Field = {FieldThis};
    Objects.CountMito = size(MitoObjects, 1);
    Objects.TMREmeanMitoIntensityPerField = mean(ch3TMRE(MitoMask));
    Objects.TMREmedianMitoIntensityPerField = median(ch3TMRE(MitoMask));
    
    if doSkel
        Objects.NodeDegreeVector = {sum(AdjacencyMatrix, 1)};
    end
    Objects.MitoPixels = sum(MitoMask(:));

    % Erosion or skel derived
    if doSkel
        Objects.MitoSkelPixels = sum(skel(:));
    end
    Objects.MitoPerimPixels = sum(MitoPerimMask(:));
    Objects.MitoBodyPixels = sum(MitoErodedMask(:)); 
    Objects.MitoBodyCount = max(MitoBodyLabelIm(:)); % Needed for invagination feature
    Objects.MitoShapeBySurface_Norm = Objects.MitoBodyPixels / Objects.MitoPerimPixels; % Roundness feature
    Objects.MitoBodycountByMitocount_Norm = Objects.MitoBodyCount / Objects.CountMito; % Invagination feature
    
    % Nuclei features
    Objects.NucleiPixels = sum(NucMask(:));
    [~, Objects.NucleiCount] = bwlabeln(NucMask, 26);
    
    % NormalizedFeatures
    if doSkel
        Objects.MitoSkelProportion_Norm = Objects.MitoSkelPixels / Objects.MitoPixels;
        Objects.MitoSkelProportion_Norm = Objects.MitoSkelPixels / Objects.MitoPixels;
        % Skeleton derived
        Objects.TotalNodeCount = size(node, 2);
        Objects.AverageNodePerMito_Norm = Objects.TotalNodeCount / Objects.CountMito;
        Objects.TotalLinkCount = size(link, 2);
        Objects.NodesPerMitoMean_Norm = Objects.TotalNodeCount / Objects.CountMito;
        Objects.LinksPerMitoMean_Norm = Objects.TotalLinkCount / Objects.CountMito;
        Objects.AverageNodeDegree_Norm = mean(Objects.NodeDegreeVector{:}); %average lenght of branch in pixels
        Objects.MedianNodeDegree_Norm = median(Objects.NodeDegreeVector{:});
        Objects.StdNodeDegree = std(Objects.NodeDegreeVector{:});
        Objects.MadNodeDegree = mad(Objects.NodeDegreeVector{:}, 1);
    end
    Objects.MitoPixelsByNuc_Norm =  Objects.MitoPixels / Objects.NucleiPixels;
    Objects.MeanMitoArea_Norm = Objects.MitoPixels / Objects.CountMito;
    Objects.CountMito_Norm = Objects.CountMito / Objects.NucleiPixels;

    Objects.MitoPerimProprtion_Norm =  Objects.MitoPerimPixels / Objects.MitoPixels;
    Objects.MitoBodyProportion_Norm = Objects.MitoBodyPixels / Objects.MitoPixels;
    Objects.MitoBodyCount_Norm = Objects.MitoBodyCount / Objects.CountMito;
    

    %Objects = Iris_AnnotateTable(Objects, Layout, {'Barcode', 'CellLine', 'ExperimentalCondition'});
    Objects = Iris_AnnotateTable(Objects, Layout, 'All');
    
    
    %% Previews
    
    MiddlePlane = round(size(ch2_MTgreen, 3)/2);
    imSize = [size(ch2_MTgreen, 1), size(ch2_MTgreen, 2)];
    [BarMask, BarCenter] = f_barMask(20, 0.17195767195767195, imSize, imSize(1)-50, 50, 7); % it(BarMask)

    MitoDisplayIm3D = mat2gray(ch2_MTgreen); %vol(MitoDisplayIm3D)
    MitoDisplayIm = imadjust(MitoDisplayIm3D(:,:,MiddlePlane)); % imtool(MitoDisplayIm)
    PreviewMito = imoverlay(MitoDisplayIm, bwperim(MitoMask(:,:,MiddlePlane)), [1 0 0]);
    PreviewMito = imoverlay(PreviewMito, BarMask, [1 1 1]); % imtool(PreviewMito)
    
    PreviewMito3D = imoverlay(imadjust(max(MitoDisplayIm3D, [], 3)), BarMask, [1 1 1]);
    PreviewMito3D = imoverlay(PreviewMito3D, BarMask, [1 1 1]); % imtool(PreviewMito3D)
    
    PreviewMitoOnly =  imoverlay(imadjust(MitoDisplayIm3D(:,:,MiddlePlane)), BarMask, [1 1 1]); % imtool(PreviewMitoOnly)

    if doSkel
        PreviewSkel = imoverlay(imadjust(max(MitoDisplayIm3D, [], 3)), max(skel, [], 3), [0 1 1]);
        PreviewSkel = imoverlay(PreviewSkel, BarMask, [1 1 1]); % imtool(PreviewSkel)
    end
    
    DapiBGVec = ch1_H(~(PreNucMask));
    DapiBackground = mean(DapiBGVec);
    DapiDisplayIm3D = medfilt3(f_ImAdjust(ch1_H-DapiBackground, 0.9999)); % vol(DapiDisplayIm3D)

    PreviewNuclei = imoverlay(mat2gray(max(DapiDisplayIm3D, [], 3)), imdilate(bwperim(max(NucMask, [], 3)),strel('disk', 2)), [0 0 1]);
    PreviewNuclei = imoverlay( PreviewNuclei, BarMask, [1 1 1]); % imtool(PreviewNuclei)
    
    PreviewTMRE = imoverlay(imadjust(max(ch3TMRE, [], 3),[0 0.1],[0 1]), bwperim(max(MitoMask, [], 3)), [1 0 0]);
    PreviewTMRE = imoverlay( PreviewTMRE, BarMask, [1 1 1]); % imtool(PreviewTMRE)
    
    PreviewMTG = imoverlay(imadjust(max(ch2_MTgreen, [], 3),[0 0.1],[0 1]), bwperim(max(MitoMask, [], 3)), [1 0 0]);
    PreviewMTG = imoverlay( PreviewMTG, BarMask, [1 1 1]); % imtool(PreviewMTG)
    
    PreviewRGB = cat(3, zeros(size(MitoDisplayIm), 'uint16'), imadjust(max(uint16(ch2_MTgreen), [], 3), [0 0.1], [0 1]), imadjust(max(uint16(ch1_H), [], 3), [0 0.05], [0 1]));
    PreviewRGB = imoverlay(PreviewRGB, BarMask, [1 1 1]); % imtool(PreviewRGB)
    
    PreviewMitoDoG = imoverlay(imadjust(max(uint16(MitoDoG),[],3)), BarMask, [1 1 1]); % imtool(PreviewMitoDoG)

    fileNameThis = [WellThis, '_', FieldThis];
    
    filename_Mito =  [PreviewPath, filesep, fileNameThis, '_Mito.png'];
    imwrite(PreviewMito, filename_Mito);
    if doSkel
        filename_Skel =  [PreviewPath, filesep, fileNameThis, '_Skel.png'];
        imwrite(PreviewSkel, filename_Skel);
    end

    filename_Dapi =  [PreviewPath, filesep, fileNameThis, '_Dapi.png'];
    imwrite(PreviewNuclei, filename_Dapi);
    filename_RGB =  [PreviewPath, filesep, fileNameThis, '_RGB.png'];
    imwrite(PreviewRGB, filename_RGB);
    
    
    filename_TMRE =  [PreviewPath, filesep, fileNameThis, '_TMRE.png'];
    imwrite(PreviewTMRE, filename_TMRE);
    filename_MTG =  [PreviewPath, filesep, fileNameThis, '_MTG.png'];
    imwrite(PreviewMTG, filename_MTG);


end

