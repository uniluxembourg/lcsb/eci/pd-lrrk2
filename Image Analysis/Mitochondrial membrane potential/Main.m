%% Collect Linux\Slurm metadata
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Job is running on node:')
[~, node] = system('hostname');
disp(node)
disp('Job is run by user:')
[~, user] = system('whoami');
disp(user)
disp('Current slurm jobs of current user:')
[~, sq] = system(['squeue -u ', user]);
disp(sq)
tic
disp(['Start: ' datestr(now, 'yyyymmdd_HHMMSS')])
disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')

DataTransfer = 0;

if DataTransfer
    error('skipping image analysis because DataTransfer = 1')
end

%% Flags

delete(gcp('nocreate'))
myCluster = parcluster('local');
AvailableWorkers = myCluster.NumWorkers;
if AvailableWorkers >= 24
    pool = parpool(12)
else
    pool = parpool(1)
end

addpath(genpath('/work/projects/lcsb_hcs/Library/hcsforge'))
addpath(genpath('/work/projects/lcsb_hcs/Library/hcsIris'))

% if ~exist('InPath') % if Inpath is provided  via command line, use that one
%     %InPath = '/mnt/IrisHCS/Data/ZlatanHodzic/ZellwegerPeroxisomes/20210216IRDProlineTest_in';
%     %InPath = '/mnt/IrisHCS/Data/ZlatanHodzic/ZellwegerPeroxisomes/ZH_20210216_CatalaseHoechstPhalloidin_60X3D_ImFocus_20210315-1800_20210318_162438_in'
%     %InPath = '/mnt/IrisHCS/Data/ZlatanHodzic/ZellwegerPeroxisomes/ZH_20210216_CatalaseHoechstPhalloidin_60X3D_ImFocus_20210315-1800_20210318_162438_in'
%     InPath = '/mnt/IrisHCS/Data/ZlatanHodzic/ZellwegerPeroxisomes/ZH_20210216_CatalaseHoechstPhalloidin_60X3D_ImFocus_20210315-2500_20210319_082419_in'
% end
% 
% MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
% MetaData = f_CV8000_getChannelInfo(InPath, MesPath);
% 
% if ~exist('OutPath') % if Outpath is provided  via command line, use that one
%     %OutPath = '/mnt/D/tmp/Zlatan202103220';
%     %OutPath = '/mnt/AtlasHCS/HCS_Platform/Data/ZlatanHodzic/ZellwegerPeroxisomes/ZH_20210216_CatalaseHoechstPhalloidin_60X3D_ImFocus_20210315-1800_20210318_162438_out';
%     OutPath = '/mnt/AtlasHCS/HCS_Platform/Data/ZlatanHodzic/ZellwegerPeroxisomes/ZH_20210216_CatalaseHoechstPhalloidin_60X3D_ImFocus_20210315-2500_20210319_082419_out';
% end


if ~exist('InPath') % if Inpath is provided  via command line, use that one
    %InPath = '/mnt/IrisHCS/Data/BrunoSantos/TMRM60x2DForAdhish/20210203_Adhish_test1_3_lines_8fields_las90_v2_20210203_124155_in'
    %InPath = '/mnt/IrisHCS/Data/BrunoSantos/TMRM60x2DForAdhish/Test2cellcarrier384ultraMT_TMRM_Hoechst_in';
    %InPath = '/mnt/AtlasHCS/YokogawaCV8000Standalone/CellPathfinder/Sandro/MFN_template_TMRE_MTgreen_20210224_ProtectMove-TMRE-Mitotracker_20210224_165345/AssayPlate_PerkinElmer_CellCarrierUltra96';
    %InPath = '/mnt/IrisHCS/Data/SylvieDelcambre/SyD_20210224_TMRE_in'
    InPath = '/mnt/IrisHCS/Data/SylvieDelcambre/SyD_20210224_TMRE_in'
end

MesPath = ls([InPath, '/*.mes']); MesPath = MesPath(1:end-1); % remove line break
MetaData = f_CV8000_getChannelInfo(InPath, MesPath);

if ~exist('OutPath') % if Outpath is provided  via command line, use that one
    %OutPath = '/mnt/D/tmp/Sylvie20210326';
    OutPath = '/mnt/IrisHCS/Data/SylvieDelcambre/SyD_20210224_TMRE_outDebug'
end

%% Prepare folders
mkdir(OutPath)
PreviewPath = [OutPath, filesep, 'Previews'];
mkdir(PreviewPath)

%% Log
%%% f_LogDependenciesLinux(mfilename, OutPath)

%% Load Metadata
ObjectsAll = {};
Layout = Iris_GetLayout(InPath);
InfoTable = MetaData.InfoTable{:};
Wells = unique(InfoTable.Well);
fieldProgress = 0;
for w = 1:numel(Wells)
    WellThis = Wells{w};
    InfoTableThisWell = InfoTable(strcmp(InfoTable.Well, WellThis),:);
    FieldsThisWell = unique(InfoTableThisWell.Field);
    for f = 1:numel(FieldsThisWell)
        fieldProgress = fieldProgress + 1;
        FieldThis = FieldsThisWell{f};
        InfoTableThisField = InfoTableThisWell(strcmp(InfoTableThisWell.Field, FieldsThisWell{f}),:);
        ChannelsThisField =  unique(InfoTableThisField.Channel);
        ImPaths = cell(1, numel(ChannelsThisField));
        for c = 1:numel(ChannelsThisField)
            ChannelThis = ChannelsThisField{c};
            InfoTableThisChannel = InfoTableThisField(strcmp(InfoTableThisField.Channel,ChannelThis),:);
            InfoTableThisChannel = sortrows(InfoTableThisChannel, 'Plane', 'ascend');
            chThisPaths = cell(numel(ChannelsThisField),1);
            for p = 1:height(InfoTableThisChannel)
                chThisPaths{p} = InfoTableThisChannel{p, 'file'}{:};
            end
            ImPaths{c} = chThisPaths;
            MesFile = MetaData.MeasurementSettingFileName;
        end
       FieldMetaData{fieldProgress} = {ImPaths, MesFile, Wells{w}, FieldsThisWell{f}};
    end
end

listFields = cell2table(vertcat(FieldMetaData{:}))
disp('Debug point')

ObjectsAllFields = {};
ObjectsAllNuc = {};

%parfor i = 1:3
parfor i = 1:numel(FieldMetaData)
% parfor i = 1:100
    try
        i
        FieldThis = FieldMetaData{i}{4};
        WellThis = FieldMetaData{i}{3};

        ch1files = sort(FieldMetaData{i}{1}{1}(:));
        ch1Collector = cellfun(@(x) imread(x), ch1files, 'UniformOutput', false);
        ch1 = cat(3,ch1Collector{:}); % vol(ch1, 0, 10000) %Hoechst

        ch2files = FieldMetaData{i}{1}{2}(:);
        ch2Collector = cellfun(@(x) imread(x), ch2files, 'UniformOutput', false);
        ch2 = cat(3,ch2Collector{:}); % vol(ch2, 0, 10000) %MTgreen
        %vi(ch2)
        
        ch3files = FieldMetaData{i}{1}{3}(:);
        ch3Collector = cellfun(@(x) imread(x), ch3files, 'UniformOutput', false);
        ch3 = cat(3,ch3Collector{:}); % vol(ch3, 0, 10000) %TMRE
        %vi(ch3)

        MesFile = FieldMetaData{i}{2};
        WellThis = FieldMetaData{i}{3};
        %[ObjectsField, ObjectsNuc] = f_imageAnalysis(ch1, ch2, WellThis, FieldThis, MesFile, PreviewPath, Layout);
        %ObjectsField = f_imageAnalysis(ch1, ch2, ch3, WellThis, FieldThis, MesFile, PreviewPath);
        ObjectsField = f_imageAnalysis(ch1, ch2, ch3, WellThis, FieldThis, MesFile, PreviewPath, Layout);
        ObjectsAllFields{i} = ObjectsField;
    catch E
        disp(E)
        continue
    end
end

Data = vertcat(ObjectsAllFields{:});
writetable(Data, [OutPath, filesep, 'data.csv'])

dataWells = varfun(@Iris_GroupByWell,Data(:,:),'GroupingVariables',{'Well'});
writetable(dataWells, [OutPath, filesep, 'dataWells.csv']);


disp('Script completed successfully')