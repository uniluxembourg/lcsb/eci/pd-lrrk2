# Parkinson's disease associated LRRK2 study in dust and serum



## patRoon non-target and suspect screeening of dust and serum (polar extracts)
The patRoon_files folder contains all necessary databases and suspect lists.

The patRoon_code folder contains the code used to run patRoon.

Further information about patRoon can be found in the [GitHub repository](https://github.com/rickhelmus/patRoon).

## Metagenomics and omics integration of dust samples
The folder "Metagenomics" contains the code related with the metagenomics analysis as well as its integration with the metabolomics/exposomics datasets (DIABLO analysis).

## LipidMatch analysis of serum (non-polar extracts)
The LipidMatch folder contains the code employed for the data analysis with LipidMatch.
LipidMatch databases and code were downloaded from the [Innovative Omics website](https://innovativeomics.com/software/lipidmatch-suite/).

## Image Analysis of cell-based experiments
The folder "Image Analysis" contains the code employed for the automated image analysis.

## Multiple Linear Regression Analysis
The folder "Multiple_Linear_Regression_Analysis" contains the code employed to investigate the reationship between the chemical levels (peak intensities) and the clinical characteristics of the participants (further details in Table S1 of the manuscript). 