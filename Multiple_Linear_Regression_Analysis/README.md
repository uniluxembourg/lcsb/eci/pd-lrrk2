# Multiple Linear Regression analysis using R and the lm function
"multiple_linear_regression_model.r" contain the code employed for the analysis.
"data.xlsx" contains the input data for the model (both normalized chemical peak intensities and clinical variables).
Further details about the clinical characteristics of the individuals can be found in the manuscript and supplementary material (Table S1).
